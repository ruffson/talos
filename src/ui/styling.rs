use crate::robotics::frames::TF;
use kiss3d::conrod::{self, position::Positionable, widget_ids};

/// A set of reasonable stylistic defaults that works for the `gui` below.
pub fn theme() -> conrod::Theme {
    use conrod::position::{Align, Padding, Position, Relative};
    conrod::Theme {
        name: "Demo Theme".to_string(),
        padding: Padding::none(),
        x_position: Position::Relative(Relative::Align(Align::Start), None),
        y_position: Position::Relative(Relative::Align(Align::Start), None),
        background_color: conrod::color::DARK_CHARCOAL,
        shape_color: conrod::color::LIGHT_CHARCOAL,
        border_color: conrod::color::BLACK,
        border_width: 0.0,
        label_color: conrod::color::WHITE,
        font_id: None,
        font_size_large: 26,
        font_size_medium: 18,
        font_size_small: 12,
        widget_styling: conrod::theme::StyleMap::default(),
        mouse_drag_threshold: 0.0,
        double_click_threshold: std::time::Duration::from_millis(500),
    }
}

// Generate a unique `WidgetId` for each widget.
widget_ids! {
    pub struct Ids {
        canvas,
        titles[],
        toggles[],
        slider[],
        title,
        tf_info,
        reset_button
    }
}

/// A demonstration of some application state we want to control with a conrod GUI.
#[derive(Default)]
pub struct DemoApp {
    robot_name: String,
    joint_values: Vec<f32>,
    joint_limits: Vec<[f32; 2]>,
    current_pose: TF,
    joint_frames_visible: Vec<bool>,
}

impl DemoApp {
    pub fn new(robot_name: String, dof: u32, joint_limits: Vec<[f32; 2]>) -> Self {
        DemoApp {
            robot_name,
            joint_values: Vec::new(),
            joint_limits,
            current_pose: TF::empty(),
            joint_frames_visible: vec![true; dof as usize],
        }
    }

    /// Getters

    pub fn joint_values(&self) -> Vec<f32> {
        // Vec::from(&self.joint_values)
        let mut my_vec = Vec::new();
        for elem in &self.joint_values {
            my_vec.push(*elem);
        }
        my_vec
    }

    pub fn joint_frames_visible(&self) -> &Vec<bool> {
        &self.joint_frames_visible
    }

    /// Setters

    pub fn set_joint_values(&mut self, joints: Vec<f32>) {
        self.joint_values = joints;
    }

    pub fn set_current_pose(&mut self, current_pose: TF) {
        self.current_pose = current_pose;
    }

    /// Controls

    fn reset_joints(&mut self) {
        let zero_joints = vec![0.; self.joint_values().len()];
        self.set_joint_values(zero_joints);
    }

    fn toggle_frame_visibility(&mut self, index: usize) {
        self.joint_frames_visible[index] = !self.joint_frames_visible[index];
    }
}

/// Instantiate a GUI demonstrating every widget available in conrod.
pub fn gui(ui: &mut conrod::UiCell, ids: &Ids, app: &mut DemoApp) {
    use conrod::{widget, Labelable, Sizeable, Widget};

    const MARGIN: conrod::Scalar = 1.0;
    const LABEL_FONT_SIZE: u32 = 8;
    const TITLE_SIZE: conrod::FontSize = 20;
    const TEXT_SIZE: conrod::FontSize = 10;
    const CANVAS_WIDTH: f64 = 150.;

    widget::Canvas::new()
        .pad(MARGIN)
        .align_left()
        .w(CANVAS_WIDTH)
        .set(ids.canvas, ui);

    widget::Text::new(&app.robot_name)
        .font_size(TITLE_SIZE)
        .top_left_of(ids.canvas)
        .align_left()
        .set(ids.title, ui);

    // Setup texts + sliders to control the joints
    for i in 0..app.joint_values.len() {
        let upper_id = if i == 0 { ids.title } else { ids.slider[i - 1] };

        let joint_name = &format!("Joint {}", i + 1);
        widget::Text::new(joint_name)
            .padded_w_of(ids.canvas, MARGIN)
            .down_from(upper_id, 8.0)
            .align_left()
            .font_size(TEXT_SIZE)
            .w(50.)
            .set(ids.titles[i], ui);

        let label = if app.joint_frames_visible()[i] {
            "Hide"
        } else {
            "Show"
        };
        for _press in widget::Toggle::new(true)
            .label(label)
            .label_font_size(5)
            .w_h(20., 13.)
            .right_from(ids.titles[i], CANVAS_WIDTH - 20. - 50. - 5.)
            .set(ids.toggles[i], ui)
        {
            app.toggle_frame_visibility(i);
        }

        let slider_label = format!("{:.2}°", app.joint_values[i].to_degrees());
        if let Some(value) = widget::Slider::new(
            app.joint_values[i].to_degrees(),
            app.joint_limits[i][0],
            app.joint_limits[i][1],
        )
        .down_from(ids.titles[i], 10.)
        .w_h(CANVAS_WIDTH, 15.)
        .label(&slider_label)
        .label_font_size(LABEL_FONT_SIZE)
        .set(ids.slider[i], ui)
        {
            app.joint_values[i] = value.to_radians();
        }
    }
    for _press in widget::Button::new()
        .label("Reset")
        .label_font_size(5)
        .down_from(ids.slider[ids.slider.len() - 1], 10.)
        .w(20.)
        .set(ids.reset_button, ui)
    {
        app.reset_joints();
    }

    widget::Text::new(&format!("{}", &app.current_pose))
        .font_size(TEXT_SIZE)
        .down_from(ids.reset_button, 10.)
        .left(-143.)
        .set(ids.tf_info, ui);
}
