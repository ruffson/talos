use crate::robotics::frames::TF;

use kiss3d::{scene::SceneNode, window::Window};
use nalgebra::{geometry::Translation3, Point3};

pub fn draw_world_frame(window: &mut Window) {
    window.draw_line(
        &Point3::origin(),
        &Point3::new(1.0, 0.0, 0.0),
        &Point3::new(1.0, 0.0, 0.0),
    );
    window.draw_line(
        &Point3::origin(),
        &Point3::new(0.0, 1.0, 0.0),
        &Point3::new(0.0, 1.0, 0.0),
    );
    window.draw_line(
        &Point3::origin(),
        &Point3::new(0.0, 0.0, 1.0),
        &Point3::new(0.0, 0.0, 1.0),
    );
}

pub fn draw_link(window: &mut Window, start_tf: &TF, end_tf: &TF, scaler: f32) {
    let start = start_tf.get_p() * scaler;
    let end = end_tf.get_p() * scaler;

    window.draw_line(
        &Point3::new(start[0], start[1], start[2]),
        &Point3::new(end[0], end[1], end[2]),
        &Point3::new(0., 0., 0.),
    );
}

pub fn draw_tf(window: &mut Window, tf: &TF, joint_sphere: &mut SceneNode, scaler: f32) {
    let p = tf.get_p() * scaler;
    let r = tf.get_r();

    // Draw frame axes
    window.draw_line(
        &Point3::new(p[0], p[1], p[2]),
        &Point3::new(p[0] + r[(0, 0)], p[1] + r[(1, 0)], p[2] + r[(2, 0)]),
        &Point3::new(1.0, 0.0, 0.0),
    );
    window.draw_line(
        &Point3::new(p[0], p[1], p[2]),
        &Point3::new(p[0] + r[(0, 1)], p[1] + r[(1, 1)], p[2] + r[(2, 1)]),
        &Point3::new(0.0, 1.0, 0.0),
    );
    window.draw_line(
        &Point3::new(p[0], p[1], p[2]),
        &Point3::new(p[0] + r[(0, 2)], p[1] + r[(1, 2)], p[2] + r[(2, 2)]),
        &Point3::new(0.0, 0.0, 1.0),
    );

    // Draw joint sphere
    joint_sphere.set_local_translation(Translation3::new(p[0], p[1], p[2]));
}
