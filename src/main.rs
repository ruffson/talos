mod robotics;
mod ui;

use robotics::models;
use ui::{draw, styling};

use kiss3d::{camera::ArcBall, light::Light, scene::SceneNode, window::Window};
use nalgebra::{base::Vector3, Point3};

const SCALER: f32 = 20.;

fn main() {
    // Setup Kiss3d

    let mut window = Window::new("TALOS: Robotics Kinematics Playground");
    let eye = Point3::new(0., -SCALER, SCALER);
    let mut arc_ball = ArcBall::new(eye, Point3::origin());
    arc_ball.set_up_axis(Vector3::z());
    window.set_light(Light::StickToCamera);
    window.set_background_color(0.5, 0.5, 0.5);

    // Setup links and
    let puma_560 = models::puma::setup_puma_560();
    let joints: Vec<f32> = vec![0., 0., 0., 0., 0., 0.];
    let mut joint_nodes: Vec<SceneNode> = Vec::new();

    // Setup Conrad
    let mut ids = styling::Ids::new(window.conrod_ui_mut().widget_id_generator());
    ids.titles.resize(
        puma_560.dof() as usize,
        &mut window.conrod_ui_mut().widget_id_generator(),
    );
    ids.slider.resize(
        puma_560.dof() as usize,
        &mut window.conrod_ui_mut().widget_id_generator(),
    );
    ids.toggles.resize(
        puma_560.dof() as usize,
        &mut window.conrod_ui_mut().widget_id_generator(),
    );

    window.conrod_ui_mut().theme = styling::theme();
    let mut app = styling::DemoApp::new(
        puma_560.model_name(),
        puma_560.dof(),
        puma_560.joint_limits(),
    );
    app.set_joint_values(joints);

    for _ in 0..puma_560.dof() {
        // Add one sphere per joint
        let mut sphere = window.add_sphere(0.07);
        sphere.set_color(0.1, 0.1, 0.1);
        joint_nodes.push(sphere);
    }

    // Render loop
    while window.render_with_camera(&mut arc_ball) {
        // The wrapping/dropping is important so window can be reused later
        // Otherwise the borrow-checker does not recognize that ui is
        // never used again and does not drop it.
        //draw conrod UI
        let mut ui = window.conrod_ui_mut().set_widgets();
        styling::gui(&mut ui, &ids, &mut app);
        let joints = app.joint_values();

        let all_tfs = puma_560.fk(joints, 6);
        app.set_current_pose(all_tfs[all_tfs.len() - 1]);

        let joint_frame_visibility = app.joint_frames_visible();
        drop(ui);

        // draw robot stuff
        draw::draw_world_frame(&mut window);
        // Calculate FK and draw TCP frame:

        // println!("Puma ik: \n{:?}\n\ n\n\n", puma_560.ik(all_tfs[all_tfs.len() - 1]));

        for (i, tf) in all_tfs.iter().enumerate() {
            if joint_frame_visibility[i] {
                draw::draw_tf(&mut window, tf, &mut joint_nodes[i], SCALER);
            }
            if i > 0 && i < all_tfs.len() - 1 {
                draw::draw_link(&mut window, &all_tfs[i], &all_tfs[i + 1], SCALER);
            }
        }
    }
}
