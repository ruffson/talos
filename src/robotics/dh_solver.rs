use super::frames::TF;
use nalgebra::Matrix4;

pub struct RobotArm {
    model_name: String,
    dh_a: Vec<f32>,
    dh_d: Vec<f32>,
    dh_alpha: Vec<f32>,
    joint_limits: Vec<[f32; 2]>,
    joint_offset: Vec<f32>,
    dof: u32,
    pub solver: Box<dyn Solver>,
}

impl RobotArm {
    pub fn new(
        model_name: String,
        dh_a: Vec<f32>,
        dh_d: Vec<f32>,
        dh_alpha: Vec<f32>,
        joint_offset: Vec<f32>,
        joint_limits: Vec<[f32; 2]>,
        solver: Box<dyn Solver>,
    ) -> RobotArm {
        assert!(
            dh_a.len() == dh_d.len()
                && dh_alpha.len() == joint_offset.len()
                && dh_a.len() == dh_alpha.len()
        );
        let dof = dh_a.len();
        RobotArm {
            model_name,
            dh_a,
            dh_d,
            dh_alpha,
            joint_offset,
            dof: dof as u32,
            joint_limits,
            solver,
        }
    }

    // pub fn ik(&self, tf: TF) {
    //     self.solver.ik(tf);
    // }

    pub fn dof(&self) -> u32 {
        self.dof
    }

    pub fn model_name(&self) -> String {
        String::from(&self.model_name)
    }

    pub fn joint_limits(&self) -> Vec<[f32; 2]> {
        let mut joint_limits: Vec<[f32; 2]> = Vec::new();
        for joint in &self.joint_limits {
            joint_limits.push(*joint);
        }
        joint_limits
    }

    pub fn get_tf_from_dh(&self, q: &[f32], idx: u32) -> TF {
        let cq = q[idx as usize].cos();
        let sq = q[idx as usize].sin();
        let a = self.dh_a[idx as usize];

        let calpha = self.dh_alpha[idx as usize].cos();
        let salpha = self.dh_alpha[idx as usize].sin();
        let d = self.dh_d[idx as usize];

        let tf = Matrix4::new(
            cq,
            -sq,
            0.0,
            a,
            sq * calpha,
            cq * calpha,
            -salpha,
            -d * salpha,
            sq * salpha,
            cq * salpha,
            calpha,
            d * calpha,
            0.0,
            0.0,
            0.0,
            1.0,
        );

        TF::new(tf)
    }

    pub fn fk(&self, q: Vec<f32>, goal_idx: u32) -> Vec<TF> {
        if goal_idx > self.dof {
            panic!("Goal index is larger than dof");
        }

        let mut q_corrected = Vec::new();
        for (i, q_i) in q.iter().enumerate() {
            q_corrected.push(q_i + self.joint_offset[i as usize]);
        }

        let mut out_tfs = Vec::new();
        // TODO: Allow a world offset that needs to be pre-multiplied here later
        let mut running_tf = TF::empty();
        for i in 0..goal_idx {
            let new_tf = self.get_tf_from_dh(&q_corrected, i);

            running_tf *= new_tf;
            out_tfs.push(running_tf);
        }

        out_tfs
    }
}

pub trait Solver {
    fn ik(&self, tcp: TF) -> Vec<Vec<f32>>;
}
