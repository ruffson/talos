#![allow(clippy::many_single_char_names)]

pub use core::f32::consts::PI;
use nalgebra::{Matrix3, Matrix4, UnitQuaternion, Vector3, U1, U3};
use std::fmt;
use std::ops::{Mul, MulAssign};

#[derive(Copy, Clone)]
pub struct TF {
    transform: Matrix4<f32>,
}

impl Default for TF {
    fn default() -> Self {
        TF::empty()
    }
}

impl Mul for TF {
    type Output = Self;
    fn mul(self, rhs: Self) -> Self {
        TF::new(self.transform * rhs.transform)
    }
}

impl MulAssign<TF> for TF {
    fn mul_assign(&mut self, rhs: TF) {
        self.transform = self.transform * rhs.transform;
    }
}

impl TF {
    pub fn new(transform: Matrix4<f32>) -> Self {
        TF { transform }
    }

    pub fn empty() -> Self {
        TF::new(Matrix4::identity())
    }

    pub fn get_p(&self) -> Vector3<f32> {
        Vector3::from(self.transform.fixed_slice::<U3, U1>(0, 3))
    }

    pub fn get_r(&self) -> Matrix3<f32> {
        Matrix3::from(self.transform.fixed_slice::<U3, U3>(0, 0))
    }

    pub fn get_quaternion(&self) -> UnitQuaternion<f32> {
        // Quaternion are of the storage order [x, y, z, w]
        UnitQuaternion::from_matrix(&self.get_r())
    }
}

impl fmt::Display for TF {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let p = self.get_p() * 1000.;
        let q = self.get_quaternion();

        write!(
            f,
            "
        Position:
            X: {:.3}mm,
            Y: {:.3}mm
            Z: {:.3}mm
        Orientation (quaternion):
            w: {:.4},
            x: {:.4},
            y: {:.4},
            z: {:.4},",
            p[0], p[1], p[2], q[3], q[0], q[1], q[2]
        )
    }
}

// pub fn direction_cosine(vec: Vector3<f32>) -> UnitQuaternion<f32> {
//     let alpha = vec[0] / vec.norm();
//     let beta = vec[1] / vec.norm();
//     let gamma = vec[2] / vec.norm();

//     let mat = Matrix3::new(
//         alpha.cos() * beta.cos(),
//         alpha.cos() * beta.sin() * gamma.sin() - alpha.sin() * gamma.cos(),
//         alpha.cos() * beta.sin() * gamma.cos() + alpha.sin() * gamma.sin(),
//         alpha.sin() * beta.cos(),
//         alpha.sin() * beta.sin() * gamma.sin() + alpha.cos() * gamma.cos(),
//         alpha.sin() * beta.sin() * gamma.cos() - alpha.cos() * gamma.sin(),
//         -beta.sin(),
//         beta.cos() * gamma.sin(),
//         beta.cos() * gamma.cos(),
//     );

//     println!(
//         "Direction: a:{}, ß:{}, g:{}",
//         alpha.to_degrees(),
//         beta.to_degrees(),
//         gamma.to_degrees()
//     );

//     UnitQuaternion::from_matrix(&mat)

//     // UnitQuaternion::from_euler_angles(alpha, beta, gamma)
// }

// #[cfg(test)]
// mod tests {
//     use super::robotics::*;

//     #[test]
//     fn test_fk() {
//             // DH parameters likely not correct!

//             }
//     }
