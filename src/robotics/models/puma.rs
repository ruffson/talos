use crate::robotics::{dh_solver::RobotArm, dh_solver::Solver, frames::TF};
pub use core::f32::consts::PI;

pub fn setup_puma_560() -> RobotArm {
    static A: [f32; 6] = [0.0, 0.0, 0.4318, 0.0, 0.0, 0.0];
    static ALPHA: [f32; 6] = [0.0, -PI / 2.0, 0.0, -PI / 2.0, PI / 2.0, -PI / 2.0];
    static D: [f32; 6] = [0.0, 0.0, 0.1397, 0.43307, 0.0, 0.05588];
    static OFFSETS: [f32; 6] = [0.0, 0.0, PI, 0.0, 0.0, PI];

    let joint_limits_deg = vec![
        [-160., 160.],
        [-180., 70.],
        [-45., 220.],
        [-150., 150.],
        [-100., 100.],
        [-266., 266.],
    ];

    struct PumaArm();
    impl Solver for PumaArm {
        /// From Craig's book: _Introduction to Robotics_, 3rd ed., page 117
        fn ik(&self, tf: TF) -> Vec<Vec<f32>> {
            let px = tf.get_p()[0];
            let py = tf.get_p()[1];
            let pz = tf.get_p()[2];

            let rot = tf.get_r();
            println!("THis is the TF: {}", tf);
            let mut solutions = Vec::new();

            // ---------
            // JOINT 1
            // ---------
            let mut q1 = Vec::new();
            q1.push(py.atan2(px) - D[2].atan2((px.powi(2) + py.powi(2) + pz.powi(2)).sqrt()));
            q1.push(py.atan2(px) - D[2].atan2(-(px.powi(2) + py.powi(2) + pz.powi(2)).sqrt()));
            println!("THETA_1: {:?}", q1);
            // ---------
            // JOINT 3
            // ---------
            let mut q3 = Vec::new();
            let k = (px.powi(2) + py.powi(2) + pz.powi(2)
                - A[2].powi(2)
                - A[3].powi(2)
                - D[2].powi(2)
                - D[3].powi(2))
                / (2. * A[2]);
            println!("K is: {}", k);
            q3.push(A[3].atan2(D[2]) - k.atan2((A[3].powi(2) + D[3].powi(2) - k.powi(2)).sqrt()));
            q3.push(A[3].atan2(D[2]) - k.atan2(-(A[3].powi(2) + D[3].powi(2) - k.powi(2)).sqrt()));
            println!("THETA_3: {:?}", q3);
            for q3i in q3.iter() {
                for q1i in q1.iter() {
                    // ---------
                    // JOINT 2
                    // ---------
                    let lhs_2 = (-A[3] - A[2] * q3i.cos()) * pz
                        - (q1i.cos() * px + q1i.sin() * py) * (D[3] - A[2] * q3i.sin());
                    let rhs_2 = (A[2] * q3i.sin() - D[3])
                        - (A[3] + A[2] * q3i.cos()) * (q1i.cos() * px + q1i.sin() * py);

                    let q23 = lhs_2.atan2(rhs_2);
                    let q2i = q23 - q3i;
                    // ---------
                    // JOINT 4
                    // ---------
                    let lhs_4 = -rot[(0, 2)] * q1i.sin() + rot[(1, 2)] * q1i.cos();
                    let rhs_4 = -rot[(0, 2)] * q1i.cos() * q23.cos() + rot[(2, 2)] * q23.sin();

                    let q4i = if !(lhs_4.abs() < 1e-5 && rhs_4.abs() < 1e-5) {
                        lhs_4.atan2(rhs_4)
                    } else {
                        0.
                    };
                    // ---------
                    // JOINT 5
                    // ---------
                    let s5 = rot[(0, 2)]
                        * (q1i.cos() * q23.cos() * q4i.cos() + q1i.sin() * q4i.sin())
                        + rot[(1, 2)] * (q1i.sin() * q23.cos() * q4i.cos() - q1i.cos() * q4i.sin())
                        - rot[(2, 2)] * (q23.sin() * q4i.cos());
                    let c5 = rot[(0, 2)] * (-q1i.cos() * q23.sin())
                        + rot[(1, 2)] * (-q1i.sin() * q23.sin())
                        + rot[(2, 2)] * (-q23.cos());
                    let q5i = -s5.atan2(c5);
                    // ---------
                    // JOINT 6
                    // ---------
                    let s6 = -rot[(0, 0)]
                        * (q1i.cos() * q23.cos() * q4i.sin() - q1i.sin() * q4i.cos())
                        - rot[(1, 0)] * (q1i.sin() * q23.cos() * q4i.sin() + q1i.cos() * q1i.cos())
                        + rot[(2, 0)] * (q23.sin() * q4i.sin());
                    let c6 = rot[(0, 0)]
                        * ((q1i.cos() * q23.cos() * q4i.cos() + q1i.sin() * q4i.sin()) * q5i.cos()
                            - q1i.cos() * q23.sin() * q5i.sin())
                        + rot[(1, 0)]
                            * ((q1i.sin() * q23.cos() * q4i.cos() - q1i.cos() * q4i.sin())
                                * q5i.cos()
                                - q1i.sin() * q23.sin() * q5i.sin())
                        - rot[(2, 0)] * (q23.sin() * q4i.cos() * q5i.cos() + q23.cos() * q5i.sin());
                    let q6i = s6.atan2(c6);

                    solutions.push(vec![*q1i, q2i, *q3i, q4i, q5i, q6i]);
                    // Add flipped solutions
                    solutions.push(vec![*q1i, q2i, *q3i, q4i + PI, -q5i, q6i + PI]);
                }
            }
            for sol in solutions.iter() {
                let deg_arr: Vec<_> = sol.iter().map(|q| q.to_degrees()).collect();
                println!("These are the solutions: \n{:?}", deg_arr);
            }
            solutions
        }
    }

    RobotArm::new(
        String::from("Puma 560"),
        A.to_vec(),
        D.to_vec(),
        ALPHA.to_vec(),
        OFFSETS.to_vec(),
        joint_limits_deg,
        Box::new(PumaArm {}),
    )
}
