# TALOS

Momentarily abandoned project and an early effort to try out Rust a while ago.

The original plan for Talos was:

1. to play around with Rust,
2. to visualize robot kinematics just from the DH parameters without the need to declare actual joint offsets and
3. to have some playground to learn Rust and play around with numeric IK solver, pow, etc. 

Reasons for momentary abandonment:
   - Early state and potential abandonment of used UI/3D libraries: conrod and kiss3d,
   - Naïve and mostly non-idiomatic early attempt at using Rust, more development would probably require a rewrite first,
   - Lack of time

What was accomplished so far: 

- Simple visualization of frames for a given set of DH parameters with a simple (and quite small) UI:

![Demo](./assets/gifs/talos_basic_demo.gif)
